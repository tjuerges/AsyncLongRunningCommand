"""This is an Asyncio Tango Device"""

from tango import DevState, GreenMode, DevDouble, DevVoid, DevString
from tango.server import Device, command, attribute, run
from datetime import datetime
import asyncio
import json
import numpy

# I want to print neat ISO timestamps.
def now():
    return datetime.utcnow().strftime('%FT%T.%f')[:-3]

class AsyncLongRunningCommand(Device):
    async def init_device(self):
        await super().init_device()

        # This is the pseudo "duration" in s of the long
        # running command.
        self._waitTime = 5.0

        # This is a current "state" of all long running commands.
        # The attribute commandState returns this value on read.
        self._commandState = json.dumps({})

        # And this is just a random number that gets renewed
        # in a coroutine every 0.1s.
        # This value is returned then the randomNumber attribute
        # is read.
        self._randomNumber = numpy.random.random()

        # If this is set to True, then a change of the
        # randomNumber attribute will be pushed to change event
        # subscribers.
        # Executing a long running command sets this to True and
        # as soon as the last long running command is done, this is
        # set to False again.
        self._doPush_randomNumber = False

        # This is for how long the coroutine which updates the
        # random number sleeps.
        self._updateSleep = 1.0

        # This signals the coroutine which modifies the random
        # number to run for as long as the value is True.
        self._run = True

        # Enable the manual sending of change events without
        # checking of the value.
        self.set_change_event('commandState', True, False)
        self.set_change_event('randomNumber', True, False)

        # Start the coroutine which updates the randomNumber
        # attribute every 1.0s.
        self._update_randomNumber_task = asyncio.create_task(self.update_randomNumber())

        # Be nice.
        self.set_state(DevState.ON)


    async def update_commandState(self, newState = None) -> None:
        '''
        Update the commandState attribute. Called as needed.
        '''
        tmpCommandState = json.loads(self._commandState)
        tmpCommandState[newState['timestamp']] = newState
        self._commandState = json.dumps(tmpCommandState)
        # Notify subscribers about the changed value.
        self.push_change_event('commandState', self._commandState)


    async def update_randomNumber(self) -> None:
        '''
        This coroutine frequently updates the randomNumber
        attribute.
        '''
        while self._run:
            # Run after self._updateSleep seconds.
            await asyncio.sleep(self._updateSleep)
            # Create a new random number.
            self._randomNumber = numpy.random.random()
            # Check if the change event should be pushed or not.
            if self._doPush_randomNumber:
                self.push_change_event('randomNumber', self._randomNumber)
            # Check if all long running commands are done. Then
            # disable pushing of the change events again.
            if self._doPush_randomNumber is True and 'running' not in self._commandState:
                self._doPush_randomNumber = False
        print(f'{now()} The randomNumber update coroutine has been stopped.')


    async def execute_long_running_command(self, whoami: DevString = None, waitTime: DevDouble = None) -> None:
        '''
        Coroutine that resembles the busy part of a long running
        command.
        '''
        # Create my own pseudo state.
        # For simplicity I use the timestamp later as key.
        myState = {'timestamp': now(), 'command': whoami, 'state': 'running'}
        # Now update the commandState attribute and send a
        # change event.
        await self.update_commandState(myState)

        # To make change visible and I enable pushing of new
        # randomNumber values.
        if self._doPush_randomNumber is False:
            self._doPush_randomNumber = True

        # Now pretend to do something while yielding the CPU.
        print(f'{now()} I am the \"{myState["timestamp"]}\" command and my state is now \"{myState}\". I will pretend to be busy for {self._waitTime}s before changing my state to \"done\".')
        await asyncio.sleep(self._waitTime)
        # Assign a different state.
        myState = {'timestamp': myState['timestamp'], 'command': whoami, 'state': 'done'}
        # And update the commandState attribute and send a
        # change event again.
        await self.update_commandState(myState)
        print(f'{now()} I am the \"{myState["timestamp"]}\" command and my state is now \"{myState}\".')


    @command(dtype_in = DevString, dtype_out = DevString)
    async def longRunningCommand(self, which: DevString = None) -> DevString:
        '''
        This is the entry point for long running commands.
        '''
        task = asyncio.create_task(self.execute_long_running_command(which))
        return f'Executing long running command "{which}"'


    @command(dtype_in = DevDouble, dtype_out = DevVoid)
    async def wait_cmd(self, sleepTime) -> DevVoid:
        '''
        A command that blocks the TC event loop in the normal
        green mode for sleepTime seconds. In asyncio green mode
        the device continues to work normal.
        '''
        print(f'{now()} The command will wait for {sleepTime}s before returning...')
        await asyncio.sleep(sleepTime)
        print(f'{now()} The command waited for {sleepTime}s.')


    @attribute(dtype = str)
    async def commandState(self) -> DevString:
        '''
        The state of running and finished commands.
        '''
        return self._commandState


    @attribute
    async def waitTime(self) -> DevDouble:
        '''
        The time that the randomNumber update coroutine will sleep
        between runs.
        '''
        return self._waitTime


    @waitTime.write
    async def waitTime(self, waitTime: DevDouble = None) -> None:
        self._waitTime = waitTime


    @attribute
    async def randomNumber(self) -> DevDouble:
        '''
        A random number that is frequently modified.
        '''
        return self._randomNumber


if __name__ == '__main__':
    run((AsyncLongRunningCommand, ), green_mode = GreenMode.Asyncio)
    print(f'\n\tTerminated at {now()}')
