# AsyncLongRunningCommand

This is an example device for Tango Controls' `asyncio` Green Mode. You can run the device in iTango or iPython with the `tango` module.

You can also run this device in the [tango-playground](https://gitlab.com/tjuerges/tango-playground.git). 

## Example output

I run the device in [tango-playground](https://gitlab.com/tjuerges/tango-playground.git)'s `iTango` container after I have added the necessary modifications to the TangoDB. The client is also `iTango` as can be seen below.

First I start the Device Server:

```python
thomas@okeanos AsyncLongRunningCommand 55: start-ds.sh AsyncLongRunningCommand.py tango-playground
Ready to accept request
```

Then I start the `iTango`session for the client. Here I load a couple of event subscription helpers to make life simple. You do not need to do that if you have your own tooling that makes quick event subscription painless:

```python
In [1]: cd /opt/playground/
/opt/playground

In [2]: from devices.toolkit import events
```

Now I connect to the device and subscribe to the the change events of the two attributes `commandState` and ``randomNumber``. Please ignore the output about failed periodic and archive event subsciptions. My helper functions are quite generic.

```python
In [3]: dp = DeviceProxy('tango-playground/AsyncLongRunningCommand/1')

In [4]: ids = events.subscribe(dp, ('commandState', 'randomNumber'))
The PERIODIC_EVENT subscription for attribute ('commandState', 'randomNumber') went somehow wrong. Continuing.
2022-03-03T20:20:29.183 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/commandstate, event = change, value = {}
2022-03-03T20:20:29.189 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/randomnumber, event = change, value = 0.860087226146624
The ARCHIVE_EVENT subscription for attribute ('commandState', 'randomNumber') went somehow wrong. Continuing.
```

So far nothing spectacular. As it is normal with event subscriptions, above can be seen that the subscriber receives immediately after the subscription change events with the current attribute values.

Now I will call the device's `longRunningCommand` five times to emulate five different commands that run in parallel:

```python
In [5]: [dp.longRunningCommand(f'Command {i}') for i in range(0, 5)]
```

This triggers some output on the device side (excuses for the malformed timestamp output, but there is something broken and I could not locate where):

```python
1970-01-01 00:33:42-03-03T20:21:09.995 I am the "2022-03-03T20:21:09.995" command and my state is now "{'timestamp': '2022-03-03T20:21:09.995', 'command': 'Command 0', 'state': 'running'}". I will pretend to be busy for 5.0s before changing my state to "done".
1970-01-01 00:33:42-03-03T20:21:09.999 I am the "2022-03-03T20:21:09.999" command and my state is now "{'timestamp': '2022-03-03T20:21:09.999', 'command': 'Command 1', 'state': 'running'}". I will pretend to be busy for 5.0s before changing my state to "done".
1970-01-01 00:33:42-03-03T20:21:10.002 I am the "2022-03-03T20:21:10.002" command and my state is now "{'timestamp': '2022-03-03T20:21:10.002', 'command': 'Command 2', 'state': 'running'}". I will pretend to be busy for 5.0s before changing my state to "done".
1970-01-01 00:33:42-03-03T20:21:10.006 I am the "2022-03-03T20:21:10.006" command and my state is now "{'timestamp': '2022-03-03T20:21:10.006', 'command': 'Command 3', 'state': 'running'}". I will pretend to be busy for 5.0s before changing my state to "done".
1970-01-01 00:33:42-03-03T20:21:10.010 I am the "2022-03-03T20:21:10.010" command and my state is now "{'timestamp': '2022-03-03T20:21:10.010', 'command': 'Command 4', 'state': 'running'}". I will pretend to be busy for 5.0s before changing my state to "done".
```

The commands have been received and are now *executed*. After about 5s the commands finish their execution:

```python
1970-01-01 00:33:42-03-03T20:21:15.002 I am the "2022-03-03T20:21:09.995" command and my state is now "{'timestamp': '2022-03-03T20:21:09.995', 'command': 'Command 0', 'state': 'done'}".
1970-01-01 00:33:42-03-03T20:21:15.002 I am the "2022-03-03T20:21:09.999" command and my state is now "{'timestamp': '2022-03-03T20:21:09.999', 'command': 'Command 1', 'state': 'done'}".
1970-01-01 00:33:42-03-03T20:21:15.003 I am the "2022-03-03T20:21:10.002" command and my state is now "{'timestamp': '2022-03-03T20:21:10.002', 'command': 'Command 2', 'state': 'done'}".
1970-01-01 00:33:42-03-03T20:21:15.007 I am the "2022-03-03T20:21:10.006" command and my state is now "{'timestamp': '2022-03-03T20:21:10.006', 'command': 'Command 3', 'state': 'done'}".
1970-01-01 00:33:42-03-03T20:21:15.011 I am the "2022-03-03T20:21:10.010" command and my state is now "{'timestamp': '2022-03-03T20:21:10.010', 'command': 'Command 4', 'state': 'done'}".
```

At the end the commands set their states do done. This triggers sending of change events.

In the meanwhile the client receives a handful of change events. First five from the `commandState` attribute that indicate the start of each command's execution.

```python
2022-03-03T20:21:09.995 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/commandstate, event = change, value = {"2022-03-03T20:21:09.995": {"timestamp": "2022-03-03T20:21:09.995", "command": "Command 0", "state": "running"}}
2022-03-03T20:21:09.999 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/commandstate, event = change, value = {"2022-03-03T20:21:09.995": {"timestamp": "2022-03-03T20:21:09.995", "command": "Command 0", "state": "running"}, "2022-03-03T20:21:09.999": {"timestamp": "2022-03-03T20:21:09.999", "command": "Command 1", "state": "running"}}
2022-03-03T20:21:10.003 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/commandstate, event = change, value = {"2022-03-03T20:21:09.995": {"timestamp": "2022-03-03T20:21:09.995", "command": "Command 0", "state": "running"}, "2022-03-03T20:21:09.999": {"timestamp": "2022-03-03T20:21:09.999", "command": "Command 1", "state": "running"}, "2022-03-03T20:21:10.002": {"timestamp": "2022-03-03T20:21:10.002", "command": "Command 2", "state": "running"}}
2022-03-03T20:21:10.007 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/commandstate, event = change, value = {"2022-03-03T20:21:09.995": {"timestamp": "2022-03-03T20:21:09.995", "command": "Command 0", "state": "running"}, "2022-03-03T20:21:09.999": {"timestamp": "2022-03-03T20:21:09.999", "command": "Command 1", "state": "running"}, "2022-03-03T20:21:10.002": {"timestamp": "2022-03-03T20:21:10.002", "command": "Command 2", "state": "running"}, "2022-03-03T20:21:10.006": {"timestamp": "2022-03-03T20:21:10.006", "command": "Command 3", "state": "running"}}
2022-03-03T20:21:10.011 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/commandstate, event = change, value = {"2022-03-03T20:21:09.995": {"timestamp": "2022-03-03T20:21:09.995", "command": "Command 0", "state": "running"}, "2022-03-03T20:21:09.999": {"timestamp": "2022-03-03T20:21:09.999", "command": "Command 1", "state": "running"}, "2022-03-03T20:21:10.002": {"timestamp": "2022-03-03T20:21:10.002", "command": "Command 2", "state": "running"}, "2022-03-03T20:21:10.006": {"timestamp": "2022-03-03T20:21:10.006", "command": "Command 3", "state": "running"}, "2022-03-03T20:21:10.010": {"timestamp": "2022-03-03T20:21:10.010", "command": "Command 4", "state": "running"}}
Out[5]:
['Executing long running command "Command 0"',
 'Executing long running command "Command 1"',
 'Executing long running command "Command 2"',
 'Executing long running command "Command 3"',
 'Executing long running command "Command 4"']
```

And then one every second for the `randomNumber` attribute.

```python
2022-03-03T20:21:10.042 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/randomnumber, event = change, value = 0.825864740855542
2022-03-03T20:21:11.040 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/randomnumber, event = change, value = 0.30966933571818844
2022-03-03T20:21:12.044 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/randomnumber, event = change, value = 0.7901337852932143
2022-03-03T20:21:13.050 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/randomnumber, event = change, value = 0.45384513461513654
2022-03-03T20:21:14.052 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/randomnumber, event = change, value = 0.9238026165185618
```

 Finally the commands are done and change their state. This triggers the sending of change events for the `commandState` attribute.

```python
2022-03-03T20:21:15.002 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/commandstate, event = change, value = {"2022-03-03T20:21:09.995": {"timestamp": "2022-03-03T20:21:09.995", "command": "Command 0", "state": "done"}, "2022-03-03T20:21:09.999": {"timestamp": "2022-03-03T20:21:09.999", "command": "Command 1", "state": "running"}, "2022-03-03T20:21:10.002": {"timestamp": "2022-03-03T20:21:10.002", "command": "Command 2", "state": "running"}, "2022-03-03T20:21:10.006": {"timestamp": "2022-03-03T20:21:10.006", "command": "Command 3", "state": "running"}, "2022-03-03T20:21:10.010": {"timestamp": "2022-03-03T20:21:10.010", "command": "Command 4", "state": "running"}}
2022-03-03T20:21:15.003 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/commandstate, event = change, value = {"2022-03-03T20:21:09.995": {"timestamp": "2022-03-03T20:21:09.995", "command": "Command 0", "state": "done"}, "2022-03-03T20:21:09.999": {"timestamp": "2022-03-03T20:21:09.999", "command": "Command 1", "state": "done"}, "2022-03-03T20:21:10.002": {"timestamp": "2022-03-03T20:21:10.002", "command": "Command 2", "state": "running"}, "2022-03-03T20:21:10.006": {"timestamp": "2022-03-03T20:21:10.006", "command": "Command 3", "state": "running"}, "2022-03-03T20:21:10.010": {"timestamp": "2022-03-03T20:21:10.010", "command": "Command 4", "state": "running"}}
2022-03-03T20:21:15.003 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/commandstate, event = change, value = {"2022-03-03T20:21:09.995": {"timestamp": "2022-03-03T20:21:09.995", "command": "Command 0", "state": "done"}, "2022-03-03T20:21:09.999": {"timestamp": "2022-03-03T20:21:09.999", "command": "Command 1", "state": "done"}, "2022-03-03T20:21:10.002": {"timestamp": "2022-03-03T20:21:10.002", "command": "Command 2", "state": "done"}, "2022-03-03T20:21:10.006": {"timestamp": "2022-03-03T20:21:10.006", "command": "Command 3", "state": "running"}, "2022-03-03T20:21:10.010": {"timestamp": "2022-03-03T20:21:10.010", "command": "Command 4", "state": "running"}}
2022-03-03T20:21:15.009 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/commandstate, event = change, value = {"2022-03-03T20:21:09.995": {"timestamp": "2022-03-03T20:21:09.995", "command": "Command 0", "state": "done"}, "2022-03-03T20:21:09.999": {"timestamp": "2022-03-03T20:21:09.999", "command": "Command 1", "state": "done"}, "2022-03-03T20:21:10.002": {"timestamp": "2022-03-03T20:21:10.002", "command": "Command 2", "state": "done"}, "2022-03-03T20:21:10.006": {"timestamp": "2022-03-03T20:21:10.006", "command": "Command 3", "state": "done"}, "2022-03-03T20:21:10.010": {"timestamp": "2022-03-03T20:21:10.010", "command": "Command 4", "state": "running"}}
2022-03-03T20:21:15.011 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/commandstate, event = change, value = {"2022-03-03T20:21:09.995": {"timestamp": "2022-03-03T20:21:09.995", "command": "Command 0", "state": "done"}, "2022-03-03T20:21:09.999": {"timestamp": "2022-03-03T20:21:09.999", "command": "Command 1", "state": "done"}, "2022-03-03T20:21:10.002": {"timestamp": "2022-03-03T20:21:10.002", "command": "Command 2", "state": "done"}, "2022-03-03T20:21:10.006": {"timestamp": "2022-03-03T20:21:10.006", "command": "Command 3", "state": "done"}, "2022-03-03T20:21:10.010": {"timestamp": "2022-03-03T20:21:10.010", "command": "Command 4", "state": "done"}}
```

 After that a final change event comes in from the `randomNumber` attribute. This is due to how I implemented the on and off switching of the change event sending for the ``randomNumber`` attribute.

```python
2022-03-03T20:21:15.053 device = AsyncLongRunningCommand(tango-playground/asynclongrunningcommand/1), attribute = tango://databaseds.tangonet:10000/tango-playground/asynclongrunningcommand/1/randomnumber, event = change, value = 0.26721798944379127
```

At last I just unsubscribe from the events.

```python
In [6]: events.unsubscribe(dp, ids)

In [7]:
```

